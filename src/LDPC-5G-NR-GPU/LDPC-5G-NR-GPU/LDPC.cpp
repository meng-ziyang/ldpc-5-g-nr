#include "LDPC.h"

#include <memory>
#include <fstream>
#include <time.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#define PI 3.141592654

// 生成正态分布随机数
double gaussrand()
{
	static double U, V;
	static int phase = 0;
	double Z;

	if (phase == 0)
	{
		U = (rand() + 1.0) / ((double)RAND_MAX + 1.0);
		V = (rand() + 1.0) / ((double)RAND_MAX + 1.0);
		Z = sqrt(-2.0 * log(U)) * sin(2.0 * PI * V);
	}
	else
	{
		Z = sqrt(-2.0 * log(U)) * cos(2.0 * PI * V);
	}

	phase = 1 - phase;
	return Z;
}

void Check_LDPC_Encode()
{
	int CheckTimes = 10;

	LDPC_Matrix matrix;

	float R = 0.2;
	for (int k = 40; k < 3840; ++k)
	{
		Load_LDPC_Matrix(matrix, k, R);

		uint8_t* Msg = new uint8_t[k];
		uint8_t* Code_Complete = new uint8_t[matrix.nbrOfCol];
		uint8_t* Check = new uint8_t[matrix.nbrOfRow];

		for (int i = 0; i < CheckTimes; ++i)
		{
			memset(Msg, 0, k * sizeof(uint8_t));
			memset(Code_Complete, 0, matrix.nbrOfCol * sizeof(uint8_t));
			memset(Check, 0, matrix.nbrOfRow * sizeof(uint8_t));

			for (int j = 0; j < k; ++j)
				Msg[j] = rand() % 2;
			LDPC_Encode(matrix, Msg, nullptr, Code_Complete);

			for (int j = 0; j < matrix.H_Complete_NonzeroElements; ++j)
				Check[matrix.H_Complete_Row_Index[j]] += Code_Complete[matrix.H_Complete_Col_Index[j]];
			for (int j = 0; j < matrix.nbrOfRow; ++j)
				Check[j] = Check[j] % 2;
			uint32_t S = 0;
			for (int j = 0; j < matrix.nbrOfRow; ++j)
				S += Check[j];

			if (S == 0)
				printf("K=%d, R=%.3f, BG=%d, a=%d, Zc=%d Check Successfully.\r",
					k, matrix.Rate, matrix.BG_Choosen, matrix.a_idx, matrix.Zc);
			else
			{
				printf("H Row, Col, BG, a, Zc\n");
				printf("%d %d %d %d %d\n", matrix.nbrOfRow, matrix.nbrOfCol, matrix.BG_Choosen, matrix.a_idx, matrix.Zc);

				for (int i = 0; i < matrix.H_NonzeroElements; ++i)
					printf("%d ", matrix.H_Row_Index[i]);
				printf("\n\n\n");
				for (int i = 0; i < matrix.H_NonzeroElements; ++i)
					printf("%d ", matrix.H_Col_Index[i]);
				printf("\n\n\n");
				printf("%d - K=%d, R=%.3f Check Failed.\n", i + 1, k, matrix.Rate);

				LDPC_Encode(matrix, Msg, nullptr, Code_Complete);
			}
		}
		Free_LDPC_Matrix(matrix);
		delete[]Msg;
		delete[]Code_Complete;
		delete[]Check;
	}

	R = 0.3333;
	for (int k = 40; k < 8192; ++k)
	{
		Load_LDPC_Matrix(matrix, k, R);

		uint8_t* Msg = new uint8_t[k];
		uint8_t* Code_Complete = new uint8_t[matrix.nbrOfCol];
		uint8_t* Check = new uint8_t[matrix.nbrOfRow];

		for (int i = 0; i < CheckTimes; ++i)
		{
			memset(Msg, 0, k * sizeof(uint8_t));
			memset(Code_Complete, 0, matrix.nbrOfCol * sizeof(uint8_t));
			memset(Check, 0, matrix.nbrOfRow * sizeof(uint8_t));

			for (int j = 0; j < k; ++j)
				Msg[j] = rand() % 2;
			LDPC_Encode(matrix, Msg, nullptr, Code_Complete);

			for (int j = 0; j < matrix.H_Complete_NonzeroElements; ++j)
				Check[matrix.H_Complete_Row_Index[j]] += Code_Complete[matrix.H_Complete_Col_Index[j]];
			for (int j = 0; j < matrix.nbrOfRow; ++j)
				Check[j] = Check[j] % 2;
			uint32_t S = 0;
			for (int j = 0; j < matrix.nbrOfRow; ++j)
				S += Check[j];

			if (S == 0)
				printf("K=%d, R=%.3f, BG=%d, a=%d, Zc=%d Check Successfully.\r",
					k, matrix.Rate, matrix.BG_Choosen, matrix.a_idx, matrix.Zc);
			else
			{
				printf("H Row, Col, BG, a, Zc\n");
				printf("%d %d %d %d %d\n", matrix.nbrOfRow, matrix.nbrOfCol, matrix.BG_Choosen, matrix.a_idx, matrix.Zc);

				for (int i = 0; i < matrix.H_NonzeroElements; ++i)
					printf("%d ", matrix.H_Row_Index[i]);
				printf("\n\n\n");
				for (int i = 0; i < matrix.H_NonzeroElements; ++i)
					printf("%d ", matrix.H_Col_Index[i]);
				printf("\n\n\n");
				printf("%d - K=%d, R=%.3f Check Failed.\n", i + 1, k, matrix.Rate);

				LDPC_Encode(matrix, Msg, nullptr, Code_Complete);
			}
		}
		Free_LDPC_Matrix(matrix);
		delete[]Msg;
		delete[]Code_Complete;
		delete[]Check;
	}

	R = 0.4;
	for (int k = 40; k < 8192; ++k)
	{
		Load_LDPC_Matrix(matrix, k, R);

		uint8_t* Msg = new uint8_t[k];
		uint8_t* Code_Complete = new uint8_t[matrix.nbrOfCol];
		uint8_t* Check = new uint8_t[matrix.nbrOfRow];

		for (int i = 0; i < CheckTimes; ++i)
		{
			memset(Msg, 0, k * sizeof(uint8_t));
			memset(Code_Complete, 0, matrix.nbrOfCol * sizeof(uint8_t));
			memset(Check, 0, matrix.nbrOfRow * sizeof(uint8_t));

			for (int j = 0; j < k; ++j)
				Msg[j] = rand() % 2;
			LDPC_Encode(matrix, Msg, nullptr, Code_Complete);

			for (int j = 0; j < matrix.H_Complete_NonzeroElements; ++j)
				Check[matrix.H_Complete_Row_Index[j]] += Code_Complete[matrix.H_Complete_Col_Index[j]];
			for (int j = 0; j < matrix.nbrOfRow; ++j)
				Check[j] = Check[j] % 2;
			uint32_t S = 0;
			for (int j = 0; j < matrix.nbrOfRow; ++j)
				S += Check[j];

			if (S == 0)
				printf("K=%d, R=%.3f, BG=%d, a=%d, Zc=%d Check Successfully.\r",
					k, matrix.Rate, matrix.BG_Choosen, matrix.a_idx, matrix.Zc);
			else
			{
				printf("H Row, Col, BG, a, Zc\n");
				printf("%d %d %d %d %d\n", matrix.nbrOfRow, matrix.nbrOfCol, matrix.BG_Choosen, matrix.a_idx, matrix.Zc);

				for (int i = 0; i < matrix.H_NonzeroElements; ++i)
					printf("%d ", matrix.H_Row_Index[i]);
				printf("\n\n\n");
				for (int i = 0; i < matrix.H_NonzeroElements; ++i)
					printf("%d ", matrix.H_Col_Index[i]);
				printf("\n\n\n");
				printf("%d - K=%d, R=%.3f Check Failed.\n", i + 1, k, matrix.Rate);

				LDPC_Encode(matrix, Msg, nullptr, Code_Complete);
			}
		}
		Free_LDPC_Matrix(matrix);
		delete[]Msg;
		delete[]Code_Complete;
		delete[]Check;
	}

	R = 0.5;
	for (int k = 40; k < 8192; ++k)
	{
		Load_LDPC_Matrix(matrix, k, R);

		uint8_t* Msg = new uint8_t[k];
		uint8_t* Code_Complete = new uint8_t[matrix.nbrOfCol];
		uint8_t* Check = new uint8_t[matrix.nbrOfRow];

		for (int i = 0; i < CheckTimes; ++i)
		{
			memset(Msg, 0, k * sizeof(uint8_t));
			memset(Code_Complete, 0, matrix.nbrOfCol * sizeof(uint8_t));
			memset(Check, 0, matrix.nbrOfRow * sizeof(uint8_t));

			for (int j = 0; j < k; ++j)
				Msg[j] = rand() % 2;
			LDPC_Encode(matrix, Msg, nullptr, Code_Complete);

			for (int j = 0; j < matrix.H_Complete_NonzeroElements; ++j)
				Check[matrix.H_Complete_Row_Index[j]] += Code_Complete[matrix.H_Complete_Col_Index[j]];
			for (int j = 0; j < matrix.nbrOfRow; ++j)
				Check[j] = Check[j] % 2;
			uint32_t S = 0;
			for (int j = 0; j < matrix.nbrOfRow; ++j)
				S += Check[j];

			if (S == 0)
				printf("K=%d, R=%.3f, BG=%d, a=%d, Zc=%d Check Successfully.\r",
					k, matrix.Rate, matrix.BG_Choosen, matrix.a_idx, matrix.Zc);
			else
			{
				printf("H Row, Col, BG, a, Zc\n");
				printf("%d %d %d %d %d\n", matrix.nbrOfRow, matrix.nbrOfCol, matrix.BG_Choosen, matrix.a_idx, matrix.Zc);

				for (int i = 0; i < matrix.H_NonzeroElements; ++i)
					printf("%d ", matrix.H_Row_Index[i]);
				printf("\n\n\n");
				for (int i = 0; i < matrix.H_NonzeroElements; ++i)
					printf("%d ", matrix.H_Col_Index[i]);
				printf("\n\n\n");
				printf("%d - K=%d, R=%.3f Check Failed.\n", i + 1, k, matrix.Rate);

				LDPC_Encode(matrix, Msg, nullptr, Code_Complete);
			}
		}
		Free_LDPC_Matrix(matrix);
		delete[]Msg;
		delete[]Code_Complete;
		delete[]Check;
	}

	R = 0.6667;
	for (int k = 40; k < 8192; ++k)
	{
		Load_LDPC_Matrix(matrix, k, R);

		uint8_t* Msg = new uint8_t[k];
		uint8_t* Code_Complete = new uint8_t[matrix.nbrOfCol];
		uint8_t* Check = new uint8_t[matrix.nbrOfRow];

		for (int i = 0; i < CheckTimes; ++i)
		{
			memset(Msg, 0, k * sizeof(uint8_t));
			memset(Code_Complete, 0, matrix.nbrOfCol * sizeof(uint8_t));
			memset(Check, 0, matrix.nbrOfRow * sizeof(uint8_t));

			for (int j = 0; j < k; ++j)
				Msg[j] = rand() % 2;
			LDPC_Encode(matrix, Msg, nullptr, Code_Complete);

			for (int j = 0; j < matrix.H_Complete_NonzeroElements; ++j)
				Check[matrix.H_Complete_Row_Index[j]] += Code_Complete[matrix.H_Complete_Col_Index[j]];
			for (int j = 0; j < matrix.nbrOfRow; ++j)
				Check[j] = Check[j] % 2;
			uint32_t S = 0;
			for (int j = 0; j < matrix.nbrOfRow; ++j)
				S += Check[j];

			if (S == 0)
				printf("K=%d, R=%.3f, BG=%d, a=%d, Zc=%d Check Successfully.\r",
					k, matrix.Rate, matrix.BG_Choosen, matrix.a_idx, matrix.Zc);
			else
			{
				printf("H Row, Col, BG, a, Zc\n");
				printf("%d %d %d %d %d\n", matrix.nbrOfRow, matrix.nbrOfCol, matrix.BG_Choosen, matrix.a_idx, matrix.Zc);

				for (int i = 0; i < matrix.H_NonzeroElements; ++i)
					printf("%d ", matrix.H_Row_Index[i]);
				printf("\n\n\n");
				for (int i = 0; i < matrix.H_NonzeroElements; ++i)
					printf("%d ", matrix.H_Col_Index[i]);
				printf("\n\n\n");
				printf("%d - K=%d, R=%.3f Check Failed.\n", i + 1, k, matrix.Rate);

				LDPC_Encode(matrix, Msg, nullptr, Code_Complete);
			}
		}
		Free_LDPC_Matrix(matrix);
		delete[]Msg;
		delete[]Code_Complete;
		delete[]Check;
	}

	R = 0.75;
	for (int k = 40; k < 8192; ++k)
	{
		Load_LDPC_Matrix(matrix, k, R);

		uint8_t* Msg = new uint8_t[k];
		uint8_t* Code_Complete = new uint8_t[matrix.nbrOfCol];
		uint8_t* Check = new uint8_t[matrix.nbrOfRow];

		for (int i = 0; i < CheckTimes; ++i)
		{
			memset(Msg, 0, k * sizeof(uint8_t));
			memset(Code_Complete, 0, matrix.nbrOfCol * sizeof(uint8_t));
			memset(Check, 0, matrix.nbrOfRow * sizeof(uint8_t));

			for (int j = 0; j < k; ++j)
				Msg[j] = rand() % 2;
			LDPC_Encode(matrix, Msg, nullptr, Code_Complete);

			for (int j = 0; j < matrix.H_Complete_NonzeroElements; ++j)
				Check[matrix.H_Complete_Row_Index[j]] += Code_Complete[matrix.H_Complete_Col_Index[j]];
			for (int j = 0; j < matrix.nbrOfRow; ++j)
				Check[j] = Check[j] % 2;
			uint32_t S = 0;
			for (int j = 0; j < matrix.nbrOfRow; ++j)
				S += Check[j];

			if (S == 0)
				printf("K=%d, R=%.3f, BG=%d, a=%d, Zc=%d Check Successfully.\r",
					k, matrix.Rate, matrix.BG_Choosen, matrix.a_idx, matrix.Zc);
			else
			{
				printf("H Row, Col, BG, a, Zc\n");
				printf("%d %d %d %d %d\n", matrix.nbrOfRow, matrix.nbrOfCol, matrix.BG_Choosen, matrix.a_idx, matrix.Zc);

				for (int i = 0; i < matrix.H_NonzeroElements; ++i)
					printf("%d ", matrix.H_Row_Index[i]);
				printf("\n\n\n");
				for (int i = 0; i < matrix.H_NonzeroElements; ++i)
					printf("%d ", matrix.H_Col_Index[i]);
				printf("\n\n\n");
				printf("%d - K=%d, R=%.3f Check Failed.\n", i + 1, k, matrix.Rate);

				LDPC_Encode(matrix, Msg, nullptr, Code_Complete);
			}
		}
		Free_LDPC_Matrix(matrix);
		delete[]Msg;
		delete[]Code_Complete;
		delete[]Check;
	}

	R = 0.8333;
	for (int k = 40; k < 8192; ++k)
	{
		Load_LDPC_Matrix(matrix, k, R);

		uint8_t* Msg = new uint8_t[k];
		uint8_t* Code_Complete = new uint8_t[matrix.nbrOfCol];
		uint8_t* Check = new uint8_t[matrix.nbrOfRow];

		for (int i = 0; i < CheckTimes; ++i)
		{
			memset(Msg, 0, k * sizeof(uint8_t));
			memset(Code_Complete, 0, matrix.nbrOfCol * sizeof(uint8_t));
			memset(Check, 0, matrix.nbrOfRow * sizeof(uint8_t));

			for (int j = 0; j < k; ++j)
				Msg[j] = rand() % 2;
			LDPC_Encode(matrix, Msg, nullptr, Code_Complete);

			for (int j = 0; j < matrix.H_Complete_NonzeroElements; ++j)
				Check[matrix.H_Complete_Row_Index[j]] += Code_Complete[matrix.H_Complete_Col_Index[j]];
			for (int j = 0; j < matrix.nbrOfRow; ++j)
				Check[j] = Check[j] % 2;
			uint32_t S = 0;
			for (int j = 0; j < matrix.nbrOfRow; ++j)
				S += Check[j];

			if (S == 0)
				printf("K=%d, R=%.3f, BG=%d, a=%d, Zc=%d Check Successfully.\r",
					k, matrix.Rate, matrix.BG_Choosen, matrix.a_idx, matrix.Zc);
			else
			{
				printf("H Row, Col, BG, a, Zc\n");
				printf("%d %d %d %d %d\n", matrix.nbrOfRow, matrix.nbrOfCol, matrix.BG_Choosen, matrix.a_idx, matrix.Zc);

				for (int i = 0; i < matrix.H_NonzeroElements; ++i)
					printf("%d ", matrix.H_Row_Index[i]);
				printf("\n\n\n");
				for (int i = 0; i < matrix.H_NonzeroElements; ++i)
					printf("%d ", matrix.H_Col_Index[i]);
				printf("\n\n\n");
				printf("%d - K=%d, R=%.3f Check Failed.\n", i + 1, k, matrix.Rate);

				LDPC_Encode(matrix, Msg, nullptr, Code_Complete);
			}
		}
		Free_LDPC_Matrix(matrix);
		delete[]Msg;
		delete[]Code_Complete;
		delete[]Check;
	}

	R = 0.8889;
	for (int k = 309; k < 8192; ++k)
	{
		Load_LDPC_Matrix(matrix, k, R);

		uint8_t* Msg = new uint8_t[k];
		uint8_t* Code_Complete = new uint8_t[matrix.nbrOfCol];
		uint8_t* Check = new uint8_t[matrix.nbrOfRow];

		for (int i = 0; i < CheckTimes; ++i)
		{
			memset(Msg, 0, k * sizeof(uint8_t));
			memset(Code_Complete, 0, matrix.nbrOfCol * sizeof(uint8_t));
			memset(Check, 0, matrix.nbrOfRow * sizeof(uint8_t));

			for (int j = 0; j < k; ++j)
				Msg[j] = rand() % 2;
			LDPC_Encode(matrix, Msg, nullptr, Code_Complete);

			for (int j = 0; j < matrix.H_Complete_NonzeroElements; ++j)
				Check[matrix.H_Complete_Row_Index[j]] += Code_Complete[matrix.H_Complete_Col_Index[j]];
			for (int j = 0; j < matrix.nbrOfRow; ++j)
				Check[j] = Check[j] % 2;
			uint32_t S = 0;
			for (int j = 0; j < matrix.nbrOfRow; ++j)
				S += Check[j];

			if (S == 0)
				printf("K=%d, R=%.3f, BG=%d, a=%d, Zc=%d Check Successfully.\r",
					k, matrix.Rate, matrix.BG_Choosen, matrix.a_idx, matrix.Zc);
			else
			{
				printf("H Row, Col, BG, a, Zc\n");
				printf("%d %d %d %d %d\n", matrix.nbrOfRow, matrix.nbrOfCol, matrix.BG_Choosen, matrix.a_idx, matrix.Zc);

				for (int i = 0; i < matrix.H_NonzeroElements; ++i)
					printf("%d ", matrix.H_Row_Index[i]);
				printf("\n\n\n");
				for (int i = 0; i < matrix.H_NonzeroElements; ++i)
					printf("%d ", matrix.H_Col_Index[i]);
				printf("\n\n\n");
				printf("%d - K=%d, R=%.3f Check Failed.\n", i + 1, k, matrix.Rate);

				LDPC_Encode(matrix, Msg, nullptr, Code_Complete);
			}
		}
		Free_LDPC_Matrix(matrix);
		delete[]Msg;
		delete[]Code_Complete;
		delete[]Check;
	}
}

bool LDPC_Encode(const LDPC_Matrix& matrix, const uint8_t* Msg, uint8_t* Code, uint8_t* Code_Complete)
{
	// Init Code Space
	uint8_t* Code_Complete_temp = new uint8_t[matrix.nbrOfCol];
	memset(Code_Complete_temp, 0, matrix.nbrOfCol);
	memcpy(Code_Complete_temp, Msg, matrix.nbrOfInfoBits * sizeof(uint8_t));
	uint8_t* P = new uint8_t[4 * matrix.Zc];
	memset(P, 0, 4 * matrix.Zc * sizeof(uint8_t));
	for (int i = 0; i < matrix.H_Base_NonzeroElements; ++i)
		P[matrix.H_Base_Row_Index[i]] += Msg[matrix.H_Base_Col_Index[i]];

	if (matrix.BG_Choosen == 1)
	{
		if (matrix.a_idx != 7)
		{
			// 1
			for (int i = 0; i < matrix.Zc; ++i)
				Code_Complete_temp[matrix.beginOfCheckBit + i] = P[0 * matrix.Zc + i] + P[1 * matrix.Zc + i] + P[2 * matrix.Zc + i] + P[3 * matrix.Zc + i];

			// 2
			for (int i = 0; i < matrix.Zc; ++i)
				Code_Complete_temp[matrix.beginOfCheckBit + matrix.Zc + i] = Code_Complete_temp[matrix.beginOfCheckBit + (1 + i) % matrix.Zc] + P[0 * matrix.Zc + i];

			// 3
			for (int i = 0; i < matrix.Zc; ++i)
				Code_Complete_temp[matrix.beginOfCheckBit + 2 * matrix.Zc + i] = Code_Complete_temp[matrix.beginOfCheckBit + i] + Code_Complete_temp[matrix.beginOfCheckBit + matrix.Zc + i] + P[matrix.Zc + i];

			// 4
			for (int i = 0; i < matrix.Zc; ++i)
				Code_Complete_temp[matrix.beginOfCheckBit + 3 * matrix.Zc + i] = Code_Complete_temp[matrix.beginOfCheckBit + 2 * matrix.Zc + i] + P[2 * matrix.Zc + i];
		}
		else
		{
			// 1
			for (int i = 0; i < matrix.Zc; ++i)
				Code_Complete_temp[matrix.beginOfCheckBit + (105 + i) % matrix.Zc] = P[0 * matrix.Zc + i] + P[1 * matrix.Zc + i] + P[2 * matrix.Zc + i] + P[3 * matrix.Zc + i];

			// 2
			for (int i = 0; i < matrix.Zc; ++i)
				Code_Complete_temp[matrix.beginOfCheckBit + matrix.Zc + i] = Code_Complete_temp[matrix.beginOfCheckBit + i] + P[0 * matrix.Zc + i];

			// 4
			for (int i = 0; i < matrix.Zc; ++i)
				Code_Complete_temp[matrix.beginOfCheckBit + 3 * matrix.Zc + i] = Code_Complete_temp[matrix.beginOfCheckBit + i] + P[3 * matrix.Zc + i];

			// 3
			for (int i = 0; i < matrix.Zc; ++i)
				Code_Complete_temp[matrix.beginOfCheckBit + 2 * matrix.Zc + i] = Code_Complete_temp[matrix.beginOfCheckBit + 3 * matrix.Zc + i] + P[2 * matrix.Zc + i];
		}
	}
	else
	{
		if (matrix.a_idx != 4 && matrix.a_idx != 8)
		{
			// 1
			for (int i = 0; i < matrix.Zc; ++i)
				Code_Complete_temp[matrix.beginOfCheckBit + (1 + i) % matrix.Zc] = P[0 * matrix.Zc + i] + P[1 * matrix.Zc + i] + P[2 * matrix.Zc + i] + P[3 * matrix.Zc + i];

			// 2
			for (int i = 0; i < matrix.Zc; ++i)
				Code_Complete_temp[matrix.beginOfCheckBit + matrix.Zc + i] = Code_Complete_temp[matrix.beginOfCheckBit + i] + P[0 * matrix.Zc + i];

			// 3
			for (int i = 0; i < matrix.Zc; ++i)
				Code_Complete_temp[matrix.beginOfCheckBit + 2 * matrix.Zc + i] = Code_Complete_temp[matrix.beginOfCheckBit + matrix.Zc + i] + P[matrix.Zc + i];

			// 4
			for (int i = 0; i < matrix.Zc; ++i)
				Code_Complete_temp[matrix.beginOfCheckBit + 3 * matrix.Zc + i] = Code_Complete_temp[matrix.beginOfCheckBit + i] + P[3 * matrix.Zc + i];
		}
		else
		{
			// 1
			for (int i = 0; i < matrix.Zc; ++i)
				Code_Complete_temp[matrix.beginOfCheckBit + i] = P[0 * matrix.Zc + i] + P[1 * matrix.Zc + i] + P[2 * matrix.Zc + i] + P[3 * matrix.Zc + i];

			// 2
			for (int i = 0; i < matrix.Zc; ++i)
				Code_Complete_temp[matrix.beginOfCheckBit + matrix.Zc + i] = Code_Complete_temp[matrix.beginOfCheckBit + (i + 1) % matrix.Zc] + P[0 * matrix.Zc + i];

			// 3
			for (int i = 0; i < matrix.Zc; ++i)
				Code_Complete_temp[matrix.beginOfCheckBit + 2 * matrix.Zc + i] = Code_Complete_temp[matrix.beginOfCheckBit + matrix.Zc + i] + P[matrix.Zc + i];

			// 4
			for (int i = 0; i < matrix.Zc; ++i)
				Code_Complete_temp[matrix.beginOfCheckBit + 3 * matrix.Zc + i] = Code_Complete_temp[matrix.beginOfCheckBit + i] + Code_Complete_temp[matrix.beginOfCheckBit + 2 * matrix.Zc + i] + P[2 * matrix.Zc + i];
		}
	}

	for (int i = 0; i < matrix.H_Expand_NonzeroElements; ++i)
		Code_Complete_temp[matrix.beginOfCheckBit + matrix.H_Expand_Row_Index[i]] += Code_Complete_temp[matrix.H_Expand_Col_Index[i]];

	for (int i = 0; i < matrix.nbrOfCol; ++i)
		Code_Complete_temp[i] = Code_Complete_temp[i] % 2;

	if (Code != nullptr)
	{
		memset(Code, 0, (matrix.nbrOfInfoBits + matrix.nbrOfCheckBits - 2 * matrix.Zc) * sizeof(uint8_t));
		memcpy(Code, Code_Complete_temp + matrix.Zc * 2, (matrix.nbrOfInfoBits - 2 * matrix.Zc) * sizeof(uint8_t));
		memcpy(Code + matrix.nbrOfInfoBits - 2 * matrix.Zc,
			Code_Complete_temp + matrix.beginOfCheckBit,
			(matrix.nbrOfCheckBits) * sizeof(uint8_t));
	}

	if (Code_Complete != nullptr)
		memcpy(Code_Complete, Code_Complete_temp, matrix.nbrOfCol * sizeof(uint8_t));

	delete[]Code_Complete_temp;
	delete[]P;
	return true;
}

bool LDPC_Decode_BP(const LDPC_Matrix& matrix, const double sigma, const uint32_t MaxItr,
	const bool isExitBeforeMaxItr, const double* Signal, uint8_t* Decode, uint32_t& ITR)
{
	double* RecvSignal = new double[matrix.nbrOfCol];
	memset(RecvSignal, 0, matrix.nbrOfCol * sizeof(double));
	memcpy(RecvSignal + 2 * matrix.Zc, Signal, (matrix.nbrOfInfoBits - 2 * matrix.Zc) * sizeof(double));
	memcpy(RecvSignal + matrix.beginOfCheckBit, Signal + matrix.nbrOfInfoBits - 2 * matrix.Zc, matrix.nbrOfCheckBits * sizeof(double));

	// 变量声明: 先验信息ci，变量节点到校验节点信息qij，校验节点到变量节点信息rji
	double* ci0 = nullptr, * qij0 = nullptr, * rji0 = nullptr;
	double* ci1 = nullptr, * qij1 = nullptr, * rji1 = nullptr;
	ci0 = new double[matrix.nbrOfCol];
	ci1 = new double[matrix.nbrOfCol];
	qij0 = new double[matrix.H_NonzeroElements];
	qij1 = new double[matrix.H_NonzeroElements];
	rji0 = new double[matrix.H_NonzeroElements];
	rji1 = new double[matrix.H_NonzeroElements];
	double* S0 = new double[matrix.nbrOfCol];
	double* S1 = new double[matrix.nbrOfCol];
	uint8_t* decode = new uint8_t[matrix.nbrOfCol];
	uint32_t* check = new uint32_t[matrix.nbrOfRow];
	memset(ci0, 0, matrix.nbrOfCol * sizeof(double));
	memset(ci1, 0, matrix.nbrOfCol * sizeof(double));
	memset(qij0, 0, matrix.H_NonzeroElements * sizeof(double));
	memset(qij1, 0, matrix.H_NonzeroElements * sizeof(double));
	memset(rji0, 0, matrix.H_NonzeroElements * sizeof(double));
	memset(rji1, 0, matrix.H_NonzeroElements * sizeof(double));

	// 初始化 ci/qij
	for (uint32_t i = 0; i < matrix.nbrOfCol; i++)
	{
		ci0[i] = 1 / (1 + exp(2 * RecvSignal[i] / (sigma * sigma)));
		ci1[i] = 1 / (1 + exp(-2 * RecvSignal[i] / (sigma * sigma)));
	}
	for (uint32_t i = 0; i < matrix.H_NonzeroElements; i++)
	{
		qij0[i] = ci0[matrix.H_Col_Index[i]];
		qij1[i] = ci1[matrix.H_Col_Index[i]];
	}

	// 迭代过程
	static time_t Time_Iteration = 0;
	static time_t Exit_Iteration = 0;
	for (uint32_t itr = 0; itr < MaxItr; itr++)
		//for (uint32_t itr = 0; itr < 40; itr++)
	{
		ITR++;
		time_t t1 = clock();
		// 更新rji
		for (uint32_t i = 0; i < matrix.H_NonzeroElements; i++)
		{
			double s = 1;
			for (uint32_t j = matrix.IdxConnectQij_idx[i * 2]; j < matrix.IdxConnectQij_idx[2 * i + 1]; j++)
			{
				// s = s * (1 - 2 * qij1[matrix.IdxConnectQij[j]]);
				s *= (qij0[matrix.IdxConnectQij[j]] - qij1[matrix.IdxConnectQij[j]]);
			}
			//rji0[i] = 0.5 * (1.0 + s);
			//rji1[i] = 1 - rji0[i];
			rji0[i] = 0.5 * (1 + s);
			rji1[i] = 0.5 * (1 - s);
		}

		// 更新qij
		for (uint32_t i = 0; i < matrix.H_NonzeroElements; i++)
		{
			double s0 = 1;
			double s1 = 1;
			for (uint32_t j = matrix.IdxConnectRji_idx[i * 2]; j < matrix.IdxConnectRji_idx[i * 2 + 1]; j++)
			{
				s0 *= rji0[matrix.IdxConnectRji[j]];
				s1 *= rji1[matrix.IdxConnectRji[j]];
			}
			qij0[i] = ci0[matrix.H_Col_Index[i]] * s0;
			qij1[i] = ci1[matrix.H_Col_Index[i]] * s1;
			//qij0[i] = qij0[i] / (qij0[i] + qij1[i]);
			//qij1[i] = 1 - qij0[i];
			if (qij0[i] + qij1[i] > 0)
			{
				qij0[i] = qij0[i] / (qij0[i] + qij1[i]);
				qij1[i] = 1 - qij0[i];
			}
			/*else
			{
				//qij0[i] = ci0[matrix.H_Col_Index[i]];
				//qij1[i] = ci1[matrix.H_Col_Index[i]];
				qij0[i] = 0.5;
				qij1[i] = 0.5;
			}*/
		}
		Time_Iteration += clock() - t1;

		// 提前退出
		if (isExitBeforeMaxItr == true)
		{
			for (uint32_t i = 0; i < matrix.nbrOfCol; ++i)
			{
				S0[i] = ci0[i];
				S1[i] = ci1[i];
			}

			for (uint32_t i = 0; i < matrix.H_NonzeroElements; ++i)
			{
				S0[matrix.H_Col_Index[i]] *= rji0[i];
				S1[matrix.H_Col_Index[i]] *= rji1[i];
			}
			for (uint32_t i = 0; i < matrix.nbrOfCol; ++i)
			{
				if (S0[i] < S1[i])
					decode[i] = 1;
				else
					decode[i] = 0;
			}
			bool isExist = true;
			memset(check, 0, matrix.nbrOfRow * sizeof(uint32_t));
			for (uint32_t i = 0; i < matrix.H_NonzeroElements; i++)
				check[matrix.H_Row_Index[i]] += decode[matrix.H_Col_Index[i]];
			for (uint32_t i = 0; i < matrix.nbrOfRow; i++)
				if (check[i] % 2 != 0)
					isExist = false;
			if (isExist)
				break;
		}
		Exit_Iteration += clock() - t1;
	}
	//printf("Time=%fs, Time_ITR=%fs, %.2f%%\n", Exit_Iteration / (double)CLOCKS_PER_SEC, Time_Iteration / (double)CLOCKS_PER_SEC, 100*Time_Iteration / (double)Exit_Iteration);

	// 判决
	for (uint32_t i = 0; i < matrix.nbrOfCol; ++i)
	{
		S0[i] = ci0[i];
		S1[i] = ci1[i];
	}

	for (uint32_t i = 0; i < matrix.H_NonzeroElements; ++i)
	{
		S0[matrix.H_Col_Index[i]] *= rji0[i];
		S1[matrix.H_Col_Index[i]] *= rji1[i];
	}
	for (uint32_t i = 0; i < matrix.nbrOfCol; ++i)
	{
		if (S0[i] < S1[i])
			Decode[i] = 1;
		else
			Decode[i] = 0;
	}

	delete[]RecvSignal;
	delete[]decode;
	delete[]check;
	delete[]ci0;
	delete[]ci1;
	delete[]qij0;
	delete[]qij1;
	delete[]rji0;
	delete[]rji1;
	delete[]S0;
	delete[]S1;

	return true;
}

bool LDPC_Decode_LayeredBP(const LDPC_Matrix& matrix, const double sigma, const uint32_t MaxItr,
	const bool isExitBeforeMaxItr, const double* Signal, uint8_t* Decode, uint32_t& ITR)
{
	double* RecvSignal = new double[matrix.nbrOfCol];
	memset(RecvSignal, 0, matrix.nbrOfCol * sizeof(double));
	memcpy(RecvSignal + 2 * matrix.Zc, Signal, (matrix.nbrOfInfoBits - 2 * matrix.Zc) * sizeof(double));
	memcpy(RecvSignal + matrix.beginOfCheckBit, Signal + matrix.nbrOfInfoBits - 2 * matrix.Zc, matrix.nbrOfCheckBits * sizeof(double));

	// 变量声明: 先验信息ci，变量节点到校验节点信息qij，校验节点到变量节点信息rji
	double* ci0 = nullptr, * qij0 = nullptr, * rji0 = nullptr;
	double* ci1 = nullptr, * qij1 = nullptr, * rji1 = nullptr;
	ci0 = new double[matrix.nbrOfCol];
	ci1 = new double[matrix.nbrOfCol];
	qij0 = new double[matrix.H_NonzeroElements];
	qij1 = new double[matrix.H_NonzeroElements];
	rji0 = new double[matrix.H_NonzeroElements];
	rji1 = new double[matrix.H_NonzeroElements];
	double* S0 = new double[matrix.nbrOfCol];
	double* S1 = new double[matrix.nbrOfCol];
	uint8_t* decode = new uint8_t[matrix.nbrOfCol];
	uint32_t* check = new uint32_t[matrix.nbrOfRow];
	memset(ci0, 0, matrix.nbrOfCol * sizeof(double));
	memset(ci1, 0, matrix.nbrOfCol * sizeof(double));
	memset(qij0, 0, matrix.H_NonzeroElements * sizeof(double));
	memset(qij1, 0, matrix.H_NonzeroElements * sizeof(double));
	memset(rji0, 0, matrix.H_NonzeroElements * sizeof(double));
	memset(rji1, 0, matrix.H_NonzeroElements * sizeof(double));

	// 初始化 ci/qij
	for (uint32_t i = 0; i < matrix.nbrOfCol; i++)
	{
		ci0[i] = 1 / (1 + exp(2 * RecvSignal[i] / (sigma * sigma)));
		ci1[i] = 1 / (1 + exp(-2 * RecvSignal[i] / (sigma * sigma)));
	}
	for (uint32_t i = 0; i < matrix.H_NonzeroElements; i++)
	{
		qij0[i] = ci0[matrix.H_Col_Index[i]];
		qij1[i] = ci1[matrix.H_Col_Index[i]];
	}

	// Iteration
	for (uint32_t itr = 0; itr < MaxItr; ++itr)
		//for (uint32_t itr = 0; itr < 40; itr++)
	{
		ITR++;
		// 遍历校验节点
		uint32_t qidx = 0;
		uint32_t ridx = 0;
		for (int cn = 0; cn < matrix.nbrOfRow; ++cn)
		{
			// Update Rji
			while (matrix.H_Row_Index[qidx] <= cn && qidx < matrix.H_NonzeroElements)
			{
				// update rji
				double s = 1;
				for (uint32_t j = matrix.IdxConnectQij_idx[qidx * 2]; j < matrix.IdxConnectQij_idx[2 * qidx + 1]; j++)
					//for (int j = matrix.IdxConnectQij_idx[2 * qidx + 1]-1; j >= (int)matrix.IdxConnectQij_idx[qidx * 2]; --j)
					//s = s * (1 - 2 * qij1[matrix.IdxConnectQij[j]]);
					s *= (qij0[matrix.IdxConnectQij[j]] - qij1[matrix.IdxConnectQij[j]]);
				//rji0[qidx] = 0.5 * (1.0 + s);
				//rji1[qidx] = 1 - rji0[qidx];
				rji0[qidx] = 0.5 * (1 + s);
				rji1[qidx] = 0.5 * (1 - s);



				// update qij
				double s0 = 1;
				double s1 = 1;
				for (uint32_t j = matrix.IdxConnectRji_idx[qidx * 2]; j < matrix.IdxConnectRji_idx[qidx * 2 + 1]; j++)
					//for (int j = matrix.IdxConnectRji_idx[qidx * 2 + 1]-1; j >= (int)matrix.IdxConnectRji_idx[qidx * 2]; --j)
				{
					s0 *= rji0[matrix.IdxConnectRji[j]];
					s1 *= rji1[matrix.IdxConnectRji[j]];
				}
				qij0[qidx] = ci0[matrix.H_Col_Index[qidx]] * s0;
				qij1[qidx] = ci1[matrix.H_Col_Index[qidx]] * s1;
				if ((qij0[qidx] + qij1[qidx]) > 0)
				{
					qij0[qidx] = qij0[qidx] / (qij0[qidx] + qij1[qidx]);
					qij1[qidx] = 1 - qij0[qidx];
				}
				++qidx;
			}
		}

		// 提前退出
		if (isExitBeforeMaxItr == true)
		{
			for (uint32_t i = 0; i < matrix.nbrOfCol; ++i)
			{
				S0[i] = ci0[i];
				S1[i] = ci1[i];
			}

			for (uint32_t i = 0; i < matrix.H_NonzeroElements; ++i)
			{
				S0[matrix.H_Col_Index[i]] *= rji0[i];
				S1[matrix.H_Col_Index[i]] *= rji1[i];
			}
			for (uint32_t i = 0; i < matrix.nbrOfCol; ++i)
			{
				if (S0[i] < S1[i])
					decode[i] = 1;
				else
					decode[i] = 0;
			}
			bool isExist = true;
			memset(check, 0, matrix.nbrOfRow * sizeof(uint32_t));
			for (uint32_t i = 0; i < matrix.H_NonzeroElements; i++)
				check[matrix.H_Row_Index[i]] += decode[matrix.H_Col_Index[i]];
			for (uint32_t i = 0; i < matrix.nbrOfRow; i++)
				if (check[i] % 2 != 0)
					isExist = false;
			if (isExist)
				break;
		}
	}

	// 判决
	for (uint32_t i = 0; i < matrix.nbrOfCol; ++i)
	{
		S0[i] = ci0[i];
		S1[i] = ci1[i];
	}

	for (uint32_t i = 0; i < matrix.H_NonzeroElements; ++i)
	{
		S0[matrix.H_Col_Index[i]] *= rji0[i];
		S1[matrix.H_Col_Index[i]] *= rji1[i];
	}
	for (uint32_t i = 0; i < matrix.nbrOfCol; ++i)
	{
		if (S0[i] < S1[i])
			Decode[i] = 1;
		else
			Decode[i] = 0;
	}

	delete[]RecvSignal;
	delete[]decode;
	delete[]check;
	delete[]ci0;
	delete[]ci1;
	delete[]qij0;
	delete[]qij1;
	delete[]rji0;
	delete[]rji1;
	delete[]S0;
	delete[]S1;

	return true;
}

bool LDPC_Decode_BP_GPU(const LDPC_Matrix& matrix, const double sigma, const uint32_t MaxItr, const bool isExitBeforeMaxItr, const double* Signal, uint8_t* Decode, uint32_t& ITR)
{
	double* RecvSignal = new double[matrix.nbrOfCol];
	memset(RecvSignal, 0, matrix.nbrOfCol * sizeof(double));
	memcpy(RecvSignal + 2 * matrix.Zc, Signal, (matrix.nbrOfInfoBits - 2 * matrix.Zc) * sizeof(double));
	memcpy(RecvSignal + matrix.beginOfCheckBit, Signal + matrix.nbrOfInfoBits - 2 * matrix.Zc, matrix.nbrOfCheckBits * sizeof(double));

	// 变量声明: 先验信息ci，变量节点到校验节点信息qij，校验节点到变量节点信息rji
	double* ci0 = nullptr, * qij0 = nullptr, * rji0 = nullptr;
	double* ci0_d = nullptr, * qij0_d = nullptr, * rji0_d = nullptr;
	double* ci1 = nullptr, * qij1 = nullptr, * rji1 = nullptr;
	double* ci1_d = nullptr, * qij1_d = nullptr, * rji1_d = nullptr;
	ci0 = new double[matrix.nbrOfCol];
	ci1 = new double[matrix.nbrOfCol];
	qij0 = new double[matrix.H_NonzeroElements];
	qij1 = new double[matrix.H_NonzeroElements];
	rji0 = new double[matrix.H_NonzeroElements];
	rji1 = new double[matrix.H_NonzeroElements];
	double* S0 = new double[matrix.nbrOfCol];
	double* S1 = new double[matrix.nbrOfCol];
	uint8_t* decode = new uint8_t[matrix.nbrOfCol];
	uint8_t* check = new uint8_t[matrix.nbrOfRow];
	uint8_t* decode_d = nullptr;
	uint8_t* check_d = nullptr;
	memset(ci0, 0, matrix.nbrOfCol * sizeof(double));
	memset(ci1, 0, matrix.nbrOfCol * sizeof(double));
	memset(qij0, 0, matrix.H_NonzeroElements * sizeof(double));
	memset(qij1, 0, matrix.H_NonzeroElements * sizeof(double));
	memset(rji0, 0, matrix.H_NonzeroElements * sizeof(double));
	memset(rji1, 0, matrix.H_NonzeroElements * sizeof(double));

	cudaMalloc(&ci0_d, matrix.nbrOfCol * sizeof(double));
	cudaMalloc(&ci1_d, matrix.nbrOfCol * sizeof(double));
	cudaMalloc(&qij0_d, matrix.H_NonzeroElements * sizeof(double));
	cudaMalloc(&qij1_d, matrix.H_NonzeroElements * sizeof(double));
	cudaMalloc(&rji0_d, matrix.H_NonzeroElements * sizeof(double));
	cudaMalloc(&rji1_d, matrix.H_NonzeroElements * sizeof(double));
	cudaMalloc(&decode_d, matrix.nbrOfCol * sizeof(uint8_t));
	cudaMalloc(&check_d, matrix.nbrOfRow * sizeof(uint8_t));


	// 初始化 ci/qij
	for (uint32_t i = 0; i < matrix.nbrOfCol; i++)
	{
		ci0[i] = 1 / (1 + exp(2 * RecvSignal[i] / (sigma * sigma)));
		ci1[i] = 1 / (1 + exp(-2 * RecvSignal[i] / (sigma * sigma)));
	}
	for (uint32_t i = 0; i < matrix.H_NonzeroElements; i++)
	{
		qij0[i] = ci0[matrix.H_Col_Index[i]];
		qij1[i] = ci1[matrix.H_Col_Index[i]];
	}

	cudaMemcpy(ci0_d, ci0, matrix.nbrOfCol * sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(ci1_d, ci1, matrix.nbrOfCol * sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(qij0_d, qij0, matrix.H_NonzeroElements * sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(qij1_d, qij1, matrix.H_NonzeroElements * sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(rji0_d, rji0, matrix.H_NonzeroElements * sizeof(double), cudaMemcpyHostToDevice);
	cudaMemcpy(rji1_d, rji1, matrix.H_NonzeroElements * sizeof(double), cudaMemcpyHostToDevice);

	// 迭代过程
	static time_t Time_Iteration = 0;
	static time_t Exit_Iteration = 0;
	for (uint32_t itr = 0; itr < MaxItr; itr++)
		//for (uint32_t itr = 0; itr < 40; itr++)
	{
		ITR++;
		uint32_t Blocks = ceil(matrix.H_NonzeroElements / (double)1024);
		time_t t1 = clock();
		// 更新rji
		BP_UpdateRji << <Blocks, 1024 >> > (qij0_d, qij1_d, matrix.Matrix_GPU->IdxConnectQij,
			matrix.Matrix_GPU->IdxConnectQij_idx, matrix.H_NonzeroElements, rji0_d, rji1_d);
		//for (uint32_t i = 0; i < matrix.H_NonzeroElements; i++)
		//{
		//	double s = 1;
		//	for (uint32_t j = matrix.IdxConnectQij_idx[i * 2]; j < matrix.IdxConnectQij_idx[2 * i + 1]; j++)
		//	{
		//		// s = s * (1 - 2 * qij1[matrix.IdxConnectQij[j]]);
		//		s *= (qij0[matrix.IdxConnectQij[j]] - qij1[matrix.IdxConnectQij[j]]);
		//	}
		//	//rji0[i] = 0.5 * (1.0 + s);
		//	//rji1[i] = 1 - rji0[i];
		//	rji0[i] = 0.5 * (1 + s);
		//	rji1[i] = 0.5 * (1 - s);
		//}

		// 更新qij
		BP_UpdateQij << <Blocks, 1024 >> > (rji0_d, rji1_d, ci0_d, ci1_d, matrix.Matrix_GPU->H_Col_Index,
			matrix.Matrix_GPU->IdxConnectRji, matrix.Matrix_GPU->IdxConnectRji_idx,
			matrix.H_NonzeroElements, matrix.nbrOfCol, qij0_d, qij1_d);
		//for (uint32_t i = 0; i < matrix.H_NonzeroElements; i++)
		//{
		//	double s0 = 1;
		//	double s1 = 1;
		//	for (uint32_t j = matrix.IdxConnectRji_idx[i * 2]; j < matrix.IdxConnectRji_idx[i * 2 + 1]; j++)
		//	{
		//		s0 *= rji0[matrix.IdxConnectRji[j]];
		//		s1 *= rji1[matrix.IdxConnectRji[j]];
		//	}
		//	qij0[i] = ci0[matrix.H_Col_Index[i]] * s0;
		//	qij1[i] = ci1[matrix.H_Col_Index[i]] * s1;
		//	//qij0[i] = qij0[i] / (qij0[i] + qij1[i]);
		//	//qij1[i] = 1 - qij0[i];
		//	if (qij0[i] + qij1[i] > 0)
		//	{
		//		qij0[i] = qij0[i] / (qij0[i] + qij1[i]);
		//		qij1[i] = 1 - qij0[i];
		//	}
		//	/*else
		//	{
		//		//qij0[i] = ci0[matrix.H_Col_Index[i]];
		//		//qij1[i] = ci1[matrix.H_Col_Index[i]];
		//		qij0[i] = 0.5;
		//		qij1[i] = 0.5;
		//	}*/
		//}
		Time_Iteration += clock() - t1;

		// 提前退出
		if (isExitBeforeMaxItr == true)
		{
			//cudaMemcpy(rji0, rji0_d, matrix.H_NonzeroElements * sizeof(double), cudaMemcpyDeviceToHost);
			//cudaMemcpy(rji1, rji1_d, matrix.H_NonzeroElements * sizeof(double), cudaMemcpyDeviceToHost);
			uint32_t Blocks = ceil(matrix.nbrOfCol / (double)1024);
			BP_Decide << <Blocks, 1024 >> > (ci0_d, ci1_d, rji0_d, rji1_d,
				matrix.Matrix_GPU->Col_Connect_idx, matrix.Matrix_GPU->Col_Connect, matrix.nbrOfCol, decode_d);
			Blocks = ceil(matrix.nbrOfRow / (double)1024);
			cudaMemset(check_d, 0, matrix.nbrOfRow * sizeof(uint8_t));
			BP_Check << <Blocks, 1024 >> > (matrix.nbrOfRow, decode_d, matrix.Matrix_GPU->H_Col_Index, matrix.Matrix_GPU->Row_Connect_idx,
				matrix.Matrix_GPU->Row_Connect, check_d);
			//cudaMemcpy(decode, decode_d, matrix.nbrOfCol * sizeof(uint8_t), cudaMemcpyDeviceToHost);
			cudaMemcpy(check, check_d, matrix.nbrOfRow * sizeof(uint8_t), cudaMemcpyDeviceToHost);

			//for (uint32_t i = 0; i < matrix.nbrOfCol; ++i)
			//{
			//	S0[i] = ci0[i];
			//	S1[i] = ci1[i];
			//}
			//for (uint32_t i = 0; i < matrix.H_NonzeroElements; ++i)
			//{
			//	S0[matrix.H_Col_Index[i]] *= rji0[i];
			//	S1[matrix.H_Col_Index[i]] *= rji1[i];
			//}
			//for (uint32_t i = 0; i < matrix.nbrOfCol; ++i)
			//{
			//	if (S0[i] < S1[i])
			//		decode[i] = 1;
			//	else
			//		decode[i] = 0;
			//}
			//
			//memset(check, 0, matrix.nbrOfRow * sizeof(uint8_t));
			//for (uint32_t i = 0; i < matrix.H_NonzeroElements; i++)
			//	check[matrix.H_Row_Index[i]] += decode[matrix.H_Col_Index[i]];
			bool isExist = true;
			for (uint32_t i = 0; i < matrix.nbrOfRow; i++)
				if (check[i] % 2 != 0)
					isExist = false;
			if (isExist)
				break;
		}
		Exit_Iteration += clock() - t1;
	}
	//printf("Time=%fs, Time_ITR=%fs, %.2f%%\n", Exit_Iteration / (double)CLOCKS_PER_SEC, Time_Iteration / (double)CLOCKS_PER_SEC, 100*Time_Iteration / (double)Exit_Iteration);

	//// 判决
	cudaMemcpy(Decode, decode_d, matrix.nbrOfCol * sizeof(uint8_t), cudaMemcpyDeviceToHost);
	//for (uint32_t i = 0; i < matrix.nbrOfCol; ++i)
	//{
	//	S0[i] = ci0[i];
	//	S1[i] = ci1[i];
	//}

	//for (uint32_t i = 0; i < matrix.H_NonzeroElements; ++i)
	//{
	//	S0[matrix.H_Col_Index[i]] *= rji0[i];
	//	S1[matrix.H_Col_Index[i]] *= rji1[i];
	//}
	//for (uint32_t i = 0; i < matrix.nbrOfCol; ++i)
	//{
	//	if (S0[i] < S1[i])
	//		Decode[i] = 1;
	//	else
	//		Decode[i] = 0;
	//}

	delete[]RecvSignal;
	delete[]decode;
	delete[]check;
	delete[]ci0;
	delete[]ci1;
	delete[]qij0;
	delete[]qij1;
	delete[]rji0;
	delete[]rji1;
	delete[]S0;
	delete[]S1;

	cudaFree(ci0_d);
	cudaFree(ci1_d);
	cudaFree(qij0_d);
	cudaFree(qij1_d);
	cudaFree(rji0_d);
	cudaFree(rji1_d);
	cudaFree(decode_d);
	cudaFree(check_d);

	return true;
}

bool Simulation_BP(const LDPC_Matrix& matrix, const uint64_t Seed, const float EbN0,
	const uint64_t MinError, const uint64_t MaxTrans, const uint32_t MaxITR,
	const bool isExitBeforeMaxItr, const bool isPrintInfo,
	uint64_t& ErrorBits, uint64_t& TransBits, uint64_t& ErrorFrames, uint64_t& TransFrames,
	float& AveITR, float& AveTime, uint32_t Debug)
{
	// Debug Log
	std::ofstream debug;
	if (Debug != 0)
		debug.open("debug.log", std::ios::app);

	// Init Random Seed
	//srand(Seed);

	// Init Statistic Result
	ErrorBits = 0;
	TransBits = 0;
	ErrorFrames = 0;
	TransFrames = 0;

	uint64_t remainFrames = MaxTrans;
	uint32_t SignalLen = matrix.nbrOfInfoBits + matrix.nbrOfCheckBits - 2 * matrix.Zc;

	// sqrt(Noise Power)
	double sigma = sqrt(1 / (pow(10.0, EbN0 / 10) * matrix.Rate) / 2.0);

	// Init Space
	uint8_t* Msg = new uint8_t[matrix.nbrOfInfoBits];
	uint8_t* Code = new uint8_t[SignalLen];
	double_t* RecvSignal = new double[SignalLen];
	uint8_t* Decode = new uint8_t[matrix.nbrOfCol];
	uint32_t ITR = 0;

	// Timer
	time_t start_time = clock();

	if (Debug != 0)
	{
		debug << "================================================================================\n";
		debug << "=====> K = " << matrix.nbrOfInfoBits << "  R = " << matrix.Rate << "\n";
		debug << "=====> BG = " << matrix.BG_Choosen << "  Kb = " << matrix.Kb << " Zc = " << matrix.Zc << " a = " << matrix.a_idx << "\n";
		debug << "=====> Eb/N0 = " << EbN0 << "dB \n";
		debug.flush();
	}

	// Begin Simulation
	time_t EncodeTime = 0;
	time_t DecodeTime = 0;
	while (remainFrames > 0)
	{
		memset(Msg, 0, matrix.nbrOfInfoBits * sizeof(uint8_t));
		memset(Code, 0, SignalLen * sizeof(uint8_t));
		memset(RecvSignal, 0, SignalLen * sizeof(double));
		memset(Decode, 0, matrix.nbrOfCol * sizeof(uint8_t));

		// Generate Random Msg
		for (int i = 0; i < matrix.nbrOfInfoBits; ++i)
			Msg[i] = rand() % 2;

		// LDPC Encode
		time_t t = clock();
		LDPC_Encode(matrix, Msg, Code, nullptr);
		EncodeTime += clock() - t;

		// Add AWGN on Code
		for (int i = 0; i < SignalLen; i++)
			RecvSignal[i] = Code[i] * 2 - 1 + gaussrand() * sigma;

		// Decode		
		t = clock();
		//LDPC_Decode_BP(matrix, sigma, MaxITR, isExitBeforeMaxItr, RecvSignal, Decode, ITR);
		LDPC_Decode_BP_GPU(matrix, sigma, MaxITR, isExitBeforeMaxItr, RecvSignal, Decode, ITR);
		DecodeTime += clock() - t;

		// Log
		int errbit = 0;
		for (int i = 0; i < matrix.nbrOfInfoBits; ++i)
			errbit += (Msg[i] != Decode[i]);
		ErrorBits += errbit;
		ErrorFrames += errbit > 0 ? 1 : 0;
		TransBits += matrix.nbrOfInfoBits;
		TransFrames += 1;


		if (Debug != 0 && errbit > 0)
		{
			debug << "--------------------------------------------------------------------------------\n";
			debug << "Error Bits = " << errbit << " Sum Error Bits =  " << ErrorBits << " Trans Bits " << TransBits;
			debug << " BER = " << ErrorBits / (double)TransBits << " FER = " << ErrorFrames / (double)TransFrames << " BP.\n";
			debug << "Info Bits:\n";
			for (int i = 0; i < matrix.nbrOfInfoBits; ++i)
				debug << (int)Msg[i];
			debug << "\n";

			debug << "Code Bits:\n";
			for (int i = 0; i < SignalLen; ++i)
				debug << (int)Code[i];
			debug << "\n";

			debug << "Recv Signal:\n";
			for (int i = 0; i < SignalLen; ++i)
				debug << RecvSignal[i] << " ";
			debug << "\n";

			debug << "Decode Info Bits:\n";
			for (int i = 0; i < matrix.nbrOfInfoBits; ++i)
				debug << (int)Decode[i];
			debug << "\n";

			debug << "Decode Complete Bits:\n";
			for (int i = 0; i < matrix.nbrOfCol; ++i)
				debug << (int)Decode[i];
			debug << "\n";
			debug.flush();
		}


		if (ErrorFrames >= MinError)
			break;
		remainFrames -= 1;

		time_t curr_time = clock();

		if (isPrintInfo == true)
			printf("EbN0=%.2f, Remain=%I64d, BER=%.3e, FER=%.3e, Error=%I64d, Trans=%I64d, AveITR=%.2f, AveTime=%f, En=%.1f%%, De=%.1f%%, RemainTime=%.2fs\r",
				EbN0,
				remainFrames, ErrorBits / (double)TransBits, ErrorFrames / (double)TransFrames,
				ErrorFrames, TransFrames, ITR / (double)TransFrames, (curr_time - start_time) / (double)CLOCKS_PER_SEC / TransFrames,
				100 * EncodeTime / (double)(EncodeTime + DecodeTime), 100 * DecodeTime / (double)(EncodeTime + DecodeTime),
				remainFrames * (curr_time - start_time) / (double)CLOCKS_PER_SEC / TransFrames);
	}
	delete[]Msg;
	delete[]Code;
	delete[]RecvSignal;
	delete[]Decode;

	AveITR = ITR / (double)TransFrames;
	AveTime = (clock() - start_time) / (double)CLOCKS_PER_SEC / TransFrames;

	if (Debug != 0)
		debug.close();

	return true;
}

bool Simulation_LayeredBP(const LDPC_Matrix& matrix, const uint64_t Seed, const float EbN0,
	const uint64_t MinError, const uint64_t MaxTrans, const uint32_t MaxITR,
	const bool isExitBeforeMaxItr, const bool isPrintInfo,
	uint64_t& ErrorBits, uint64_t& TransBits, uint64_t& ErrorFrames, uint64_t& TransFrames,
	float& AveITR, float& AveTime, uint32_t Debug)
{
	// Debug Log
	std::ofstream debug;
	if (Debug != 0)
		debug.open("debug.log", std::ios::app);

	// Init Random Seed
	//srand(Seed);

	// Init Statistic Result
	ErrorBits = 0;
	TransBits = 0;
	ErrorFrames = 0;
	TransFrames = 0;

	uint64_t remainFrames = MaxTrans;
	uint32_t SignalLen = matrix.nbrOfInfoBits + matrix.nbrOfCheckBits - 2 * matrix.Zc;

	// sqrt(Noise Power)
	double sigma = sqrt(1 / (pow(10.0, EbN0 / 10) * matrix.Rate) / 2.0);

	// Init Space
	uint8_t* Msg = new uint8_t[matrix.nbrOfInfoBits];
	uint8_t* Code = new uint8_t[SignalLen];
	double_t* RecvSignal = new double[SignalLen];
	uint8_t* Decode = new uint8_t[matrix.nbrOfCol];
	uint32_t ITR = 0;

	// Timer
	time_t start_time = clock();

	if (Debug != 0)
	{
		debug << "================================================================================\n";
		debug << "=====> K = " << matrix.nbrOfInfoBits << "  R = " << matrix.Rate << "\n";
		debug << "=====> BG = " << matrix.BG_Choosen << "  Kb = " << matrix.Kb << " Zc = " << matrix.Zc << " a = " << matrix.a_idx << "\n";
		debug << "=====> Eb/N0 = " << EbN0 << "dB \n";
		debug.flush();
	}

	// Begin Simulation
	time_t EncodeTime = 0;
	time_t DecodeTime = 0;
	while (remainFrames > 0)
	{
		memset(Msg, 0, matrix.nbrOfInfoBits * sizeof(uint8_t));
		memset(Code, 0, SignalLen * sizeof(uint8_t));
		memset(RecvSignal, 0, SignalLen * sizeof(double));
		memset(Decode, 0, matrix.nbrOfCol * sizeof(uint8_t));

		// Generate Random Msg
		for (int i = 0; i < matrix.nbrOfInfoBits; ++i)
			Msg[i] = rand() % 2;

		// LDPC Encode
		time_t t = clock();
		LDPC_Encode(matrix, Msg, Code, nullptr);
		EncodeTime += clock() - t;

		// Add AWGN on Code
		for (int i = 0; i < SignalLen; i++)
			RecvSignal[i] = Code[i] * 2 - 1 + gaussrand() * sigma;

		// Decode		
		t = clock();
		LDPC_Decode_LayeredBP(matrix, sigma, MaxITR, isExitBeforeMaxItr, RecvSignal, Decode, ITR);
		DecodeTime += clock() - t;

		// Log
		int errbit = 0;
		for (int i = 0; i < matrix.nbrOfInfoBits; ++i)
			errbit += (Msg[i] != Decode[i]);
		ErrorBits += errbit;
		ErrorFrames += errbit > 0 ? 1 : 0;
		TransBits += matrix.nbrOfInfoBits;
		TransFrames += 1;


		if (Debug != 0 && errbit > 0)
		{
			debug << "--------------------------------------------------------------------------------\n";
			debug << "Error Bits = " << errbit << " Sum Error Bits =  " << ErrorBits << " Trans Bits " << TransBits;
			debug << " BER = " << ErrorBits / (double)TransBits << " FER = " << ErrorFrames / (double)TransFrames << " Layered BP.\n";
			debug << "Info Bits:\n";
			for (int i = 0; i < matrix.nbrOfInfoBits; ++i)
				debug << (int)Msg[i];
			debug << "\n";

			debug << "Code Bits:\n";
			for (int i = 0; i < SignalLen; ++i)
				debug << (int)Code[i];
			debug << "\n";

			debug << "Recv Signal:\n";
			for (int i = 0; i < SignalLen; ++i)
				debug << RecvSignal[i] << " ";
			debug << "\n";

			debug << "Decode Info Bits:\n";
			for (int i = 0; i < matrix.nbrOfInfoBits; ++i)
				debug << (int)Decode[i];
			debug << "\n";

			debug << "Decode Complete Bits:\n";
			for (int i = 0; i < matrix.nbrOfCol; ++i)
				debug << (int)Decode[i];
			debug << "\n";
			debug.flush();
		}


		if (ErrorFrames >= MinError)
			break;
		remainFrames -= 1;

		time_t curr_time = clock();

		if (isPrintInfo == true)
			printf("EbN0=%.2f, Remain=%I64d, BER=%.3e, FER=%.3e, Error=%I64d, Trans=%I64d, AveITR=%.2f, AveTime=%f, En=%.1f%%, De=%.1f%%\n",
				EbN0,
				remainFrames, ErrorBits / (double)TransBits, ErrorFrames / (double)TransFrames,
				ErrorFrames, TransFrames, ITR / (double)TransFrames, (curr_time - start_time) / (double)CLOCKS_PER_SEC / TransFrames,
				100 * EncodeTime / (double)(EncodeTime + DecodeTime), 100 * DecodeTime / (double)(EncodeTime + DecodeTime));
	}
	delete[]Msg;
	delete[]Code;
	delete[]RecvSignal;
	delete[]Decode;

	AveITR = ITR / (double)TransFrames;
	AveTime = (clock() - start_time) / (double)CLOCKS_PER_SEC / TransFrames;

	if (Debug != 0)
		debug.close();

	return true;
}

__global__ void BP_UpdateRji(const double* qij0, const double* qij1,
	const uint32_t* IdxConnectQij, const uint32_t* Qij_idx, const uint32_t NumOfNonzero, double* rji0, double* rji1)
{
	uint32_t idx = blockIdx.x * blockDim.x + threadIdx.x;

	if (idx >= NumOfNonzero)
		return;

	double s = 1;
	for (uint32_t i = Qij_idx[(idx % NumOfNonzero) * 2]; i < Qij_idx[2 * (idx % NumOfNonzero) + 1]; i++)
	{
		s *= (qij0[IdxConnectQij[i]] - qij1[IdxConnectQij[i]]);
		//printf("%d = %f %f\n", idx, qij0[IdxConnectQij[i]], qij1[IdxConnectQij[i]]);
	}
	rji0[idx] = 0.5 * (1 + s);
	rji1[idx] = 0.5 * (1 - s);


}

__global__ void BP_UpdateQij(const double* rji0, const double* rji1, const double* ci0, const double* ci1,
	const uint32_t* IdxCol, const uint32_t* IdxConnectRji, const uint32_t* Rji_idx,
	const uint32_t NumOfNonzero, const uint32_t Col, double* qij0, double* qij1)
{
	uint32_t idx = blockIdx.x * blockDim.x + threadIdx.x;
	if (idx >= NumOfNonzero)
		return;

	double s0 = 1;
	double s1 = 1;
	for (uint32_t i = Rji_idx[(idx % NumOfNonzero) * 2]; i < Rji_idx[(idx % NumOfNonzero) * 2 + 1]; i++)
	{
		s0 *= rji0[IdxConnectRji[i]];
		s1 *= rji1[IdxConnectRji[i]];
	}
	qij0[idx] = ci0[IdxCol[idx % NumOfNonzero]] * s0;
	qij1[idx] = ci1[IdxCol[idx % NumOfNonzero]] * s1;
	if (qij0[idx] + qij1[idx] > 0)
	{
		qij0[idx] = qij0[idx] / (qij0[idx] + qij1[idx]);
		qij1[idx] = 1 - qij0[idx];
	}
}

__global__ void BP_Decide(const double* ci0, const double* ci1, const double* rji0, const double* rji1,
	const uint32_t* Col_Connect_idx, const uint32_t* Col_Connect,
	const uint32_t Col, uint8_t* decide)
{
	uint32_t idx = blockIdx.x * blockDim.x + threadIdx.x;
	if (idx >= Col)
		return;

	double s0 = ci0[idx];
	double s1 = ci1[idx];
	for (int i = Col_Connect_idx[2 * idx]; i < Col_Connect_idx[2 * idx + 1]; ++i)
	{
		s0 *= rji0[Col_Connect[i]];
		s1 *= rji1[Col_Connect[i]];
	}
	if (s0 < s1)
		decide[idx] = 1;
	else
		decide[idx] = 0;
}

__global__ void BP_Check(const uint32_t Row, const uint8_t* decide, const uint32_t* H_Col_Index,
	const uint32_t* Row_Connect_idx, const uint32_t* Row_Connect,
	uint8_t* check)
{
	uint32_t idx = blockIdx.x * blockDim.x + threadIdx.x;
	if (idx >= Row)
		return;

	for (int i = Row_Connect_idx[2 * idx]; i < Row_Connect_idx[2 * idx + 1]; ++i)
	{
		check[idx] += decide[H_Col_Index[Row_Connect[i]]];
	}
}
